package com.example.admin.picassotest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.up_image);

        //Picasso.with(getApplicationContext()).load(R.mipmap.ic_launcher).into(imageView);

        /*Picasso.with(getApplicationContext())
                .load(R.mipmap.ic_launcher)
                .resize(450, 450)
                .centerCrop()
                .into(imageView);*/

        /*String url = "give your image url";
        Picasso.with(getApplicationContext())
                .load(url)
                .resize(450, 450)
                .centerCrop()
                .into(imageView);*/

        Picasso.with(getApplicationContext()).load(R.drawable.capture).resize(650, 750).into(imageView);

        //Picasso.with(context).load("file:///android_asset/DvpvklR.png").into(imageView2);
        //Picasso.with(context).load(new File(...)).into(imageView3);
        //Picasso.with(context).load(new File(...)).into(imageView3);

    }

}
