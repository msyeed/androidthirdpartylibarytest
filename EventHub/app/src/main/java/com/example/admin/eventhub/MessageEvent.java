package com.example.admin.eventhub;

/**
 * Created by Mahbubul on 2.9.2015.
 */
public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
