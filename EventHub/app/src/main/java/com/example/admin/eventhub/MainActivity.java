package com.example.admin.eventhub;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    Button btnMsg;
    Button btnNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMsg = (Button) findViewById(R.id.button);
        btnNumber = (Button) findViewById(R.id.button2);

        btnMsg.setOnClickListener(this);
        btnNumber.setOnClickListener(this);
    }


    /**
     * register to event bus as a listener on start
     */
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    /**
     * Unregister from event bus on stop.
     */
    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }



    // This method will be called when a MessageEvent is posted
    public void onEvent(MessageEvent event){
        Toast.makeText(getApplicationContext(), event.message, Toast.LENGTH_SHORT).show();
    }

    // This method will be called when a SomeOtherEvent is posted
    public void onEvent(CallingEvent event){
        Toast.makeText(getApplicationContext(), event.calingNumber, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button:
                EventBus.getDefault().post(new MessageEvent("Hello everyone!"));
                break;
            case R.id.button2:
                EventBus.getDefault().post(new CallingEvent("2441139"));
                break;
        }
    }
}
