package com.example.admin.butterknifetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.Bind;
import butterknife.BindColor;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    //annotate views
    @Bind(R.id.textView) TextView txtName;
    @Bind(R.id.textView2) TextView txtEmail;
    @Bind(R.id.button) Button btnChangeText;


    //annotate resources
    @BindColor(R.color.yellow) int yellow;
    @BindColor(R.color.red) int red;

    //other available resource binds
    //@BindBool, @BindColor, @BindDimen, @BindDrawable, @BindInt, @BindString,


    //grouping vies.
    @Bind({ R.id.textView, R.id.textView2}) List<TextView> txtViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //bind butter knife
        ButterKnife.bind(this);

        txtName.setText("Rajit");
        txtEmail.setText("me.cse");

        txtEmail.setTextColor(yellow);


        //act on group vies.
        final ButterKnife.Action<View> CHANGE_COLOR = new ButterKnife.Action<View>() {
            @Override public void apply(View view, int index) {
                ((TextView)view).setTextColor(red);
            }
        };

        ButterKnife.apply(txtViews, CHANGE_COLOR);

    }


    @OnClick(R.id.button)
    public void submit() {
        Toast.makeText(getApplicationContext(), "I am pressed!", Toast.LENGTH_LONG).show();
    }


    @OnClick({ R.id.button2, R.id.button3 })
    public void pickDoor(Button abutton) {
        if (abutton.getId() == R.id.button2){
            Toast.makeText(this, "I am button 2!", Toast.LENGTH_LONG).show();
        }
        else if (abutton.getId() == R.id.button3){
            Toast.makeText(this, "I am button 3!", Toast.LENGTH_LONG).show();
        }
    }

}
